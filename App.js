import React, {Component} from 'react';
import {StyleSheet, Text, View, Button} from 'react-native';

export default class App extends Component {
  state = {
    seconds: 0,
    started: false,
    paused: false,
    timerId: null,
  }

  _start = () =>{
    if (this.state.started) return null;
    const intervalId = setInterval( () => {
      if(this.state.paused) return null;
      this.setState( prev =>{
        return {seconds: prev.seconds+1}
      })
    }, 1000);

    this.setState( prev => {
      return { started: true, paused: false, timerId: intervalId  }
    });
  }

  _pause = () => {
    this.setState( prev =>{
      return { paused: !prev.paused }
    })
  }

  _reset = () => {
    clearInterval(this.state.timerId)
    this.setState( prev => {
      return { seconds: 0, started: false, paused: false };
    })
  }

  render() {
    let pause_resume_text = this.state.paused? 'Resume' : 'Pause';
    return (
      <View style={styles.container}>
        <View style={styles.display}>
          <Text style={styles.seconds} >{this.state.seconds}</Text>
        </View>
        <View style={styles.actions}>
          <Button title="Start" onPress={ () => this._start() } disabled={this.state.started}/>
          <Button title={pause_resume_text} onPress={ () => this._pause() }/>
          <Button title="Reset" onPress={ () => this._reset() }/>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    margin:25,
    flex: 1,
  },
  display:{
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  actions:{
    flex: 1,
    flexDirection:'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  seconds:{
    fontSize: 100,
  }
})